<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('hello');
});

Route::resource('peliculas', 'PeliculasController');
Route::resource('cines', 'CinesController');
Route::resource('carteleras','CartelerasController');
Route::resource('formatoPeliculas', 'FormatoPeliculasController');
Route::resource('salas', 'SalasController');
Route::resource('tipoSalas', 'TipoSalasController');
