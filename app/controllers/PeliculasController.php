<?php

class PeliculasController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		return Pelicula::all();
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try{
			$pelicula = Pelicula::find($id);
			$statusCode = 200;
			$response = [ "pelicula" => [
				'id' => (int) $id,
				'titulo' => $pelicula->titulo,
				'sinopsis' => $pelicula->sinopsis,
				'trailer_url' => $pelicula->trailer_url,
				'image' => (int) $pelicula->image,
				'fechaEstreno' => $pelicula->fechaEstreno,
				'rated' => $pelicula->rated,
				'genero' => $pelicula->genero
			]];

		}catch(Exception $e){
			$response = [
				"error" => "File doesn`t exists"
			];
			$statusCode = 404;
		}finally{
			return Response::json($response, $statusCode);
		}

	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
